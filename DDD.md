**Design-Develop-Decision-Matrix:**

In order to meet the standards by every stage of prototyping and development of the instrument, we must keep check based on some metrics, which in turn dependent on several health/biomedical factors that are already provided by Global & National institutions. To know more please visit the [Wiki](https://gitlab.com/fshm/covid19-ventilators/-/wikis/Introduction) page. 

Here the following table represents a matrix that ties both the biomedical/health/safety standard parameters with the actual systems that integrate to form the instrument. Any system can accomodate given number of parameters. The more the control over the parameters the more the resilent and fail safe, but also poses additional development/design complexity and cost. It is not mandatory to have all to be controlled by every sub systems. But to carefully design and implement a decision making guideline is necessary and this matrix can be a utility for that.

|                | **TV** | **RR** | **I/E** | **PRE** | 
|----------------|--------|--------|---------|---------|
| **Mechanical** |        |        |         |         |
| **Electrical** |        |        |         |         |
| **Electronic** |        |        |         |         |
| **S/W or F/W** |        |        |         |         |

**TV** - Tidal Volume

**RR** - Respiratory Rate

**I/E** - Inhale/Exhale

**PRE.** - Pressure
