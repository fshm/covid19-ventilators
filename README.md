We know the growing spread of Covid19. However, the public-health-care systems are not scalable with such short duration, will suffer from demand 
of high supply of highly essential intensive care instruments. One of them is ventilators.

Ventilators are of very low availability as we know. (Exact or approximate statistics would be nice !)

To meet the availability need, several maker communities are using diverse approaches working with institutes, universities, manufacturers to prototype, 
test, validate and manufacture the ventilators.

Given the potential threshold for crisis, which might break soon, the ventilators need to be made openly, that suits local usage and production, all the while 
satisfying basic safety standards.

Please populate necessary repos, documents, datasheets, etc...

Visit the [wiki](https://gitlab.com/fshm/covid19-ventilators/-/wikis/Introduction), to learn, contribute the content